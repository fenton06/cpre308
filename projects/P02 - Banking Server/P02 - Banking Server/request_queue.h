
#ifndef REQUEST_QUEUE_H
#define REQUEST_QUEUE_H

#include <sys/time.h>

#define MAX_NUM_ACCOUNTS 10

// Types of requests a user can make
enum request {
    CHECK = 0,
    TRANS = 1,
};

// Node to hold request info
typedef struct node {
    int id;
    struct timeval time;
    int num_acts;
    int *accounts;
    int *amounts;
    enum request req;
    struct node *next;
    struct node *prev;
} req_t;

// Request queue data struct
typedef struct queue {
    int numElements;
    req_t *front;
    req_t *back;
} queue_t;

// Initialize queue - return 1 on success, 0 if error
int init_queue();

// Add request to queue - return 1 on success, 0 if error
int enqueue(req_t req);

// Remove request from queue - return NULL pointer if queue empty
req_t *dequeue();

// Get queue size (number of requests)
int queue_size();

#endif
