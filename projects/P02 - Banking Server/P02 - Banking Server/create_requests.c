
#include <stdlib.h>
#include <string.h>
#include <wordexp.h>
#include "create_requests.h"

/*
 * Parse user input and create request item to be queued.
 *
 * INPUT: cmd - user input
 * INPUT: id - request ID
 * INPUT: req - requeset object to store values
 * RETURN: 1 on success, 0 otherwise
 */
int create_request(wordexp_t *cmd, int id, req_t *req) {

    //  Check for valid command
    if (0 == strcmp(cmd->we_wordv[0], "CHECK")) {

        // Check for account ID
        if (cmd->we_wordc < 2) {
            return 0;
        }

        // Copy ID and request type
        req->id = id;
        req->req = CHECK;

        // Parse and add accounts to request
        if (!add_accounts(cmd, req)) {
            return 0;
        }

        return 1;
    } else if (0 == strcmp(cmd->we_wordv[0], "TRANS")) {

        // Make for account ID and amount
        if (cmd->we_wordc < 3) return 0;
        // Copy ID and request type
        req->id = id;
        req->req = TRANS;
        // Add accounts
        if (!add_accounts(cmd, req)) return 0;
        // Add amounts
        if (!add_amounts(cmd, req)) return 0;
        // Order accounts and amounts
        order_req(req);
        return 1;
    } else { // Invalid command
        return 0;
    }
}

/*
 * Parses user input for amounts and add to request
 *
 * INPUT:	req_t *req - Request
 * RETURN:	1 if success, 0 if error
 */
int add_amounts(wordexp_t *cmd, req_t *req) {

    // Determine number of amounts
    int n = (cmd->we_wordc - 1) / 2;

    if (n > MAX_NUM_ACCOUNTS){
        n = MAX_NUM_ACCOUNTS;
    }

    // Allocate array to store amounts
    int *ptr;
    ptr = (int *) malloc(n * sizeof(int));

    if (NULL == ptr){
        return 0;
    }

    // Parse amounts and save to array */
    int tmp, i, j;

    for (i = 0, j = 2; i < n; i++, j += 2) {
        tmp = strtol(cmd->we_wordv[j], NULL, 10);
        ptr[i] = tmp;
    }

    // Save array to request
    req->amounts = ptr;
    return 1;
}

/*
 * Parses user input for account IDs and add to request
 *
 * INPUT:	wordexp_t *cmd - User input
 * INPUT:	req_t *req - Request
 * RETURN:	1 if success, 0 if error
 */
int add_accounts(wordexp_t *cmd, req_t *req) {

    int n;

    // Determine request type, and get number of accounts
    if (req->req == CHECK) {
        n = 1;
    } else {

        n = (cmd->we_wordc - 1) / 2;

        if (n > MAX_NUM_ACCOUNTS){
            n = MAX_NUM_ACCOUNTS;
        }
    }

    // Allocate account ID array
    int *ptr;
    ptr = (int *) malloc(n * sizeof(int));

    if (NULL == ptr){
        return 0;
    }

    // Parse input and save to array
    int tmp;
    if (req->req == CHECK) {

        tmp = strtol(cmd->we_wordv[1], NULL, 10);
        if (tmp < 1) {
            free(ptr);
            return 0;
        }
        ptr[0] = tmp;
    } else {

        int i, j;

        for (i = 0, j = 1; i < n; i++, j += 2) {
            tmp = strtol(cmd->we_wordv[j], NULL, 10);
            if (tmp < 1) {
                free(ptr);
                return 0;
            }
            ptr[i] = tmp;
        }
    }

    // Save array and number of accounts to request
    req->num_acts = n;
    req->accounts = ptr;
    return 1;
}

/* Sorts account ID array in ascending order, and moves amounts to maintain same index
 *
 * INPUT:	req_t *req - Request with unordered account array
 */
void order_req(req_t *req) {

    // Check if size is larger than 1
    if (req->num_acts <= 1) {
        return;
    }

    int i, j, n;
    int min, index, amt;

    // Selection sort
    n = req->num_acts;

    for (i = 0; i < n; i++) {
        min = req->accounts[i];
        index = i;
        amt = req->amounts[i];

        for (j = i; j < n; j++) {

            if (req->accounts[j] < min) {
                min = req->accounts[j];
                index = j;
                amt = req->amounts[j];
            }
        }

        /* swap i and min */
        req->accounts[index] = req->accounts[i];
        req->accounts[i] = min;
        /* also need to switch amounts to keep it lined up with order of accounts */
        req->amounts[index] = req->amounts[i];
        req->amounts[i] = amt;
    }
    return;
}
