
#include <stdlib.h>
#include <pthread.h>
#include "Bank.h"

// Pthread mutex to lock access to all accounts
pthread_mutex_t mutex;

// Number of accounts
int n_accounts = 0;

/*
 * Initialize accounts and mutex
 *
 * RETURN 1 on success, 0 otherwise
 */
int init_accounts(int n) {

    // Initialize accounts array
    if (!initialize_accounts(n)){
        return 0;
    }

    // Initialize mutex
    pthread_mutex_init(&mutex, NULL);

    // Store number of accounts
    n_accounts = n;

    return 1;
}

/*
 * Get balance of specified account.
 *
 * INPUT:	account_id - account ID to check
 * RETURN: value of account on success, -1 otherwise
 */
int balance(int account_id) {

    // Check for valid account ID
    if (account_id < 1 || account_id > n_accounts){
        return -1;
    }

    // Acquire lock on accounts
    pthread_mutex_lock(&mutex);

    // Get current balance
    int value;
    value = read_account(account_id);

    // Unlock accounts
    pthread_mutex_unlock(&mutex);

    return value;
}

/*
 * Attempt to transfer funds from accounts.
 *
 * INPUT: accounts - array of account IDs
 * INPUT: amounts - corresponding array of transfer values
 * INPUT: n - number of accounts in the transfer request
 * RETURN: -1 on success, otherwise account ID of account with insufficient funds
 */
int transfer_funds(int *accounts, int *amounts, int n) {

    // Lock accounts
    pthread_mutex_lock(&mutex);

    int i;
    // Check all accounts have specified funds
    int bal, trans_val;
    char isf = 0;
    int isf_id = 0;

    for (i = 0; i < n; i++) {
        // Add monies!!
        trans_val = amounts[i];

        if (trans_val >= 0){
            continue;
        }

        // Check for sufficient funds
        bal = read_account(accounts[i]);

        if ((-1 * trans_val) > bal) {
            isf = 1;
            isf_id = accounts[i];
            break;
        }
    }

    // Make transfers
    if (isf == 0) {

        for (i = 0; i < n; i++) {
            trans_val = amounts[i];
            bal = read_account(accounts[i]);
            write_account(accounts[i], bal + trans_val);
        }
    }

    // Unlock accounts
    pthread_mutex_unlock(&mutex);

    if (isf == 0) {
        return -1;
    } else
        return isf_id;
}
