
#ifndef CREATE_REQUESTS_H
#define CREATE_REQUESTS_H

#include <wordexp.h>
#include "request_queue.h"

/*
 * Create a process request. Takes line of user input (wordexpt_t *cmd), a request ID (int id),
 * and a req_t pointer (req_t *req) that has already been allocated in memory.
 *
 * Returns 1 on successful parse, 0 otherwise.
 */
int create_request(wordexp_t *cmd, int id, req_t *req);

/*
 * Parse an input string for account IDs, allocates memory for an array of these account IDs,
 * and points the req_t to array.
 *
 * Returns 1 on successful parse, 0 otherwise.
 */
int add_accounts(wordexp_t *cmd, req_t *req);

/*
 * Performs same as add_accounts(), but parses amounts instead of account IDs.
 */
int add_amounts(wordexp_t *cmd, req_t *req);

/*
 * Sorts account ID array in ascending order, and moves amounts to maintain same index
 */
void order_req(req_t *req);

#endif