
#include <stdlib.h>
#include <sys/time.h>
#include "request_queue.h"

// Request queue pointer
queue_t *req_queue;

/*
 * Allocate queue and initialize to 0
 *
 * RETURN: 1 on success, 0 on failure
 */

int init_queue() {

    req_queue = (queue_t *) malloc(sizeof(queue_t));

    if (NULL == req_queue) {
        return 0;
    }

    // malloc succeeded
    req_queue->numElements = 0;
    req_queue->front = NULL;
    req_queue->back = NULL;
    return 1;
}

/*
 * Add request to queue
 *
 * RETURN: 1 on success, 0 on failure
 */
int enqueue(req_t req) {

    req_t *request;
    request = (req_t *) malloc(sizeof(req_t));

    if (NULL == request)
        return 0;

    // Copy node
    request->id = req.id;
    gettimeofday(&(request->time), NULL);
    request->num_acts = req.num_acts;
    request->accounts = req.accounts;
    request->amounts = req.amounts;
    request->req = req.req;
    request->next = NULL;
    request->prev = NULL;

    // Check if only item in queue
    if (0 == req_queue->numElements) {
        req_queue->front = request;
        req_queue->back = request;
    } else {
        req_queue->back->prev = request;
        request->next = req_queue->back;
        req_queue->back = request;
    }

    req_queue->numElements++;
    return 1;
}

/* Remove a request from queue
 *
 * RETURN: Pointer to request that was removed from queue
 */
req_t *dequeue() {

    if (0 == req_queue->numElements) {
        return NULL;
    }

    req_t *tmp;
    tmp = req_queue->front;
    req_queue->front = tmp->prev;
    req_queue->numElements--;
    return tmp;
}

/* Get queue size (number of requests)
 *
 * RETURN: Number of request in the queue
 */
int queue_size() {
    return req_queue->numElements;
}
