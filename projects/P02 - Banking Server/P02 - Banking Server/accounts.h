
#ifndef ACCOUNTS_H
#define ACCOUNTS_H

/*
 * Initialize bank accounts array and mutexes
 *
 * Returns 1 on success, 0 otherwise
 */
int init_accounts(int n);

/*
 * Get the balance of the specified account
 *
 * Returns balance on success, -1 otherwise
 */
int balance(int account_id);

/*
 * Transfer funds between n accounts
 *
 * Returns -1 on success, otherwise returns account ID for account with insufficient funds
 */
int transfer_funds(int *accounts, int *amounts, int n);

#endif