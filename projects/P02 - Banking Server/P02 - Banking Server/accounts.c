
#include <stdlib.h>
#include <pthread.h>
#include "Bank.h"

// Pthread mutex array for locking individual accounts
pthread_mutex_t *mutex;

// Number of accounts
int n_accounts = 0;

/*
 * Initialize account and mutexes array
 *
 * RETURN 1 on success, 0 otherwise
 */
int init_accounts(int n) {

    // Initialize accounts array
    if (!initialize_accounts(n)) {
        return 0;
    }

    // Allocate mutex array
    mutex = (pthread_mutex_t *) malloc(n * sizeof(pthread_mutex_t));
    if (NULL == mutex) {
        return 0;
    }

    // Initialize mutexes
    int i;

    for (i = 0; i < n; i++) {
        if (pthread_mutex_init(&mutex[i], NULL)) {
            return 0;
        }
    }

    // Store number of accounts
    n_accounts = n;

    return 1;
}

/*
 * Get balance of specified account.
 *
 * INPUT:	account_id - account ID to check
 * RETURN: value of account on success, -1 otherwise
 */
int balance(int account_id) {

    // Check for valid account ID
    if (account_id < 1 || account_id > n_accounts) {
        return -1;
    }

    // Acquire lock on account
    pthread_mutex_lock(&mutex[account_id - 1]);

    // Get current balance
    int value;
    value = read_account(account_id);

    // Unlock account
    pthread_mutex_unlock(&mutex[account_id - 1]);

    return value;
}

/*
 * Attempt to transfer funds from accounts.
 *
 * INPUT: accounts - array of account IDs
 * INPUT: amounts - corresponding array of transfer values
 * INPUT: n - number of accounts in the transfer request
 * RETURN: -1 on success, otherwise account ID of account with insufficient funds
 */
int transfer_funds(int *accounts, int *amounts, int n) {

    int i;
    // Lock all accounts
    for (i = 0; i < n; i++) {
        pthread_mutex_lock(&mutex[accounts[i] - 1]);
    }

    // Check all accounts have specified funds
    int bal, trans_val;
    char isf = 0;
    int isf_id = 0;

    for (i = 0; i < n; i++) {
        // Add monies!!
        trans_val = amounts[i];

        if (trans_val >= 0) {
            continue;
        }

        // Check for sufficient funds
        bal = read_account(accounts[i]);

        if ((-1 * trans_val) > bal) {
            isf = 1;
            isf_id = accounts[i];
            break;
        }
    }

    // Make transfers
    if (isf == 0) {

        for (i = 0; i < n; i++) {
            trans_val = amounts[i];
            bal = read_account(accounts[i]);
            write_account(accounts[i], bal + trans_val);
        }
    }

    // Unlock all accounts
    for (i = 0; i < n; i++) {
        pthread_mutex_unlock(&mutex[accounts[i] - 1]);
    }

    if (isf == 0) {
        return -1;
    } else {
        return isf_id;
    }
}
