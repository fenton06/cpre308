
/*
 * To compile: use command "make"
 *
 * To clean: use command "make clean"
 */


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <wordexp.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>
#include "request_queue.h"
#include "create_requests.h"
#include "accounts.h"

/*
 * Function prototypes
 */
void *process_request(void *arg);


/*
 * Global vars
 */
pthread_mutex_t queue_m;
pthread_mutex_t prog_term_m;
FILE *out;
char prog_term = 0;


int main(int argc, char **argv) {

    /*
     * Input verification
     */

    // Check number of args
    if (argc != 4) {
        printf("Usage: ./server <# of worker threads> <# of accounts> <output file>\n");
        return 1;
    }

    //Convert string to ints
    int num_workers, num_accounts;
    num_workers = (int) strtol(argv[1], NULL, 10);
    num_accounts = (int) strtol(argv[2], NULL, 10);

    // Validate input for number of workers and accounts
    if (num_workers <= 0) {
        printf("Invalid argument '%s'. Value must be greater than zero.\n", argv[1]);
        return 1;
    }

    if (num_accounts <= 0) {
        printf("Invalid argument '%s'. Value must be greater than zero.\n", argv[2]);
        return 1;
    }

    // Output file
    out = fopen(argv[3], "w");

    if (NULL == out) {
        perror("");
        return 1;
    }

    /*
     * Initialization
     */

    // Init bank accounts
    int err;
    err = init_accounts(num_accounts);

    if (!err) {
        printf("Error initializing bank accounts.\n");
        return 1;
    }

    // Init request queue
    err = init_queue();

    if (!err) {
        printf("Error initializing request queue.\n");
        return 1;
    }

    // Init mutexes
    pthread_mutex_init(&queue_m, NULL);
    pthread_mutex_init(&prog_term_m, NULL);

    // Init worker threads
    pthread_t workers[num_workers];
    int i;

    for (i = 0; i < num_workers; i++) {
        pthread_create(&workers[i], NULL, process_request, (void *) out);
    }

    // User input buffer and counter
    size_t nbytes = 0;
    int bytes_read;
    wordexp_t cmd;
    int id = 1;

    // Init user input buffer
    char *input = (char *) malloc(nbytes + 1);

    if (NULL == input) {
        printf("Malloc failed.\n");
        return 1;
    }

    // Init local request var that builds new requests
    req_t *req = (req_t *) malloc(sizeof(req_t));

    if (NULL == req) {
        printf("Malloc failed.\n");
        return 1;
    }

    /*
     * Main program loop
     */

    while (1) {

        printf("> ");
        bytes_read = getline(&input, &nbytes, stdin);
        /* Remove trailing newline character */
        input[strlen(input) - 1] = 0;

        /* Input error checking */
        if (wordexp(input, &cmd, 0)) {
            printf("Illegal character detected.\n");
            continue;
        } else if (cmd.we_wordc == 0) {
            continue;
        }

        /* Check if we received a program exit command */
        if (0 == strcmp(cmd.we_wordv[0], "END")) {
            break;
        }

        /* Attempt to create and queue request */
        err = create_request(&cmd, id, req);

        if (!err) {
            printf("Unable to process request.\n");
            continue;
        } else {
            printf("< ID %d\n", id++);
            /* Lock the queue */
            pthread_mutex_lock(&queue_m);
            /* Queue the request */
            enqueue(*req);
            /* Unlock the queue */
            pthread_mutex_unlock(&queue_m);
        }
    }

    /*
     * Clean up after END
     */

    // Tell threads time to terminate
    pthread_mutex_lock(&prog_term_m);
    prog_term = 1;
    pthread_mutex_unlock(&prog_term_m);

    // Join all threads
    for (i = 0; i < num_workers; i++) {
        pthread_join(workers[i], NULL);
    }

    // Free memory (not required since program ending)
    fclose(out);
    free(input);
    free(req);
    pthread_mutex_destroy(&queue_m);
    pthread_mutex_destroy(&prog_term_m);

    return 0;
}

/*
 * Worker thread
 */

void *process_request(void *arg) {

    FILE *fp = (FILE *) arg;
    req_t *req;
    int bal, err;
    struct timeval finish;

    while (1) {

        // Random wait to avoid starvation
        srand((unsigned int) time(NULL));
        usleep((useconds_t) rand() % 10000);

        // Acquire lock on request queue
        pthread_mutex_lock(&queue_m);

        // CHeck if there are requests that need processed
        if (0 == queue_size()) {
            pthread_mutex_unlock(&queue_m);
            // Check program termination signal
            pthread_mutex_lock(&prog_term_m);

            if (prog_term != 0) {
                pthread_mutex_unlock(&prog_term_m);
                break;
            }

            pthread_mutex_unlock(&prog_term_m);
            continue;
        } else {
            req = dequeue();
        }

        // Unlock request queue
        pthread_mutex_unlock(&queue_m);

        // Execute request and print to file
        if (req->req == CHECK) {
            // Get account balance
            bal = balance(req->accounts[0]);

            // Record request finish time
            gettimeofday(&finish, NULL);

            // Print results to output file
            fprintf(fp, "%d BAL %d TIME %d.%06d %d.%06d\n", req->id, bal, (int) req->time.tv_sec,
                    (int) req->time.tv_usec, (int) finish.tv_sec, (int) finish.tv_usec);
        } else {
            // Attempt to transfer funds
            err = transfer_funds(req->accounts, req->amounts, req->num_acts);

            // Record transfer finish time
            gettimeofday(&finish, NULL);

            // Check for sufficient funds and print to file
            if (err == -1) {
                fprintf(fp, "%d OK TIME %d.%06d %d.%06d\n", req->id, (int) req->time.tv_sec, (int) req->time.tv_usec,
                        (int) finish.tv_sec, (int) finish.tv_usec);
            } else {
                fprintf(fp, "%d ISF %d TIME %d.%06d %d.%06d\n", req->id, err, (int) req->time.tv_sec,
                        (int) req->time.tv_usec, (int) finish.tv_sec, (int) finish.tv_usec);
            }
        }

        // Free request after processing
        free(req);
    }

    return NULL;
}
