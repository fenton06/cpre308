#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

#define MAXINPUTSIZE 100

int main(int argc, char **argv) {

    // Default prompt
    char *prompt = "308sh> ";

    // Arrs to store input, directory, env vars, and env vals
    char input[MAXINPUTSIZE];
    char dir[MAXINPUTSIZE];
    char envVar[MAXINPUTSIZE];
    char envVal[MAXINPUTSIZE];

    // Initialize arrs
    int initi;
    for (initi = 0; initi < MAXINPUTSIZE; initi++) {
        input[initi] = '\0';
        dir[initi] = '\0';
        envVar[initi] = '\0';
        envVal[initi] = '\0';
    }

    // Status variable for execvp
    int status;

    // Vars to hold 0 in child processes
    int child;
    int child2;

    // Counters
    int i, k;

    // Current working directory
    char cwd[1024];

    // Var to hold whether or not command is a background process
    int backProc = 0;

    /*
     * INITIALIZATION
     */

    // Check number of arguments (1 or 3 only valid vals for this program)
    if (argc != 1 && argc != 3) {
        printf("Invalid number of arguments. Exiting.\n");
        return 0;
    }

    // Check for prompt value (first arg must be -p)
    if (argc == 3) {
        if (strncmp("-p", argv[1], 2) != 0) {
            printf("Invalid command line argument. Exiting.\n");
            return 0;
        }

        // Assign arg value to prompt
        prompt = argv[2];
    }


    /*
     * LOOP UNTIL EXIT
     */

    while (1) {
        // Wait for input
        fprintf(stderr, "%s", prompt);

        // Get input and ensure it has a length  > 0 and null terminate
        fgets(input, MAXINPUTSIZE, stdin);
        if ((strlen(input) > 0) && (input[strlen(input) - 1] == '\n')) {
            input[strlen(input) - 1] = '\0';
        }

        /*
         * BUILT IN COMMANDS
         */
        // Exit
        if (strncmp("exit", input, 4) == 0) {
            return 0;
        }
            //PID - process ID
        else if (strncmp("pid", input, 3) == 0) {
            printf("Process ID of this shell: %d\n", getpid());
        }
            // PPID - parent process ID
        else if (strncmp("ppid", input, 4) == 0) {
            printf("Process ID of this shell's parent: %d\n", getppid());
        }
            // CD - change to specified directory
        else if (strncmp("cd", input, 2) == 0) {
            if (strlen(input) == 2) {
                chdir(getenv("HOME"));
            } else {
                // Skip the space and get rest of input
                for (i = 3; i < strlen(input); i++) {
                    dir[i - 3] = input[i];
                }
                // Null terminate
                dir[i + 1] = '\0';

                if (chdir(dir) == -1) {
                    printf("Cannot find directory\n");
                } else {
                    chdir(dir);
                }
            }
        }
            // PWD - print working directory
        else if (strncmp("pwd", input, 3) == 0) {
            if (getcwd(cwd, 1024) == NULL) {
                printf("Failed to print current working directory");
            }
            printf("%s\n", cwd);
        }
            // SET - set environment variable
        else if (strncmp("set", input, 3) == 0 && input[3] == ' ') {

            // Initialize k - End of loop if k < -1 fails, k = -1 clears envVar, k > -1 sets envVar
            k = -2;
            for (i = 4; i < strlen(input); i++) {
                // Check for at least 1 argument
                if (k == -2 && input[i] != ' ') {
                    k = -1;
                }

                // Determine argument count at space char
                if (input[i] == ' ') {
                    // If k != -1 and we hit a space -> invalid num args
                    if (k != -1 || i == strlen(input) - 1) {
                        printf("Invalid number of arguments for set\n");
                        k = -3;
                    } else {
                        k = 0;
                    }
                }
                    // Fill envVar if k == -1
                else if (k == -1) {
                    envVar[i - 4] = input[i];
                }
                    // Fill envVal if k == -1
                else {
                    envVal[k] = input[i];
                    k++;

                    // Null terminate at end
                    if (i == strlen(input) - 1) {
                        envVal[k] = '\0';
                    }
                }
            }

            // Error messages
            if (k == -2) {
                printf("Invalid number of arguments for set\n");
            } else if (k == -1) {
                if (setenv(envVar, NULL, 1) == -1) {
                    printf("Failed to clear environment variable\n");
                }
            } else {
                if (setenv(envVar, envVal, 1) == -1) {
                    printf("Failed to set environment variable\n");
                }
            }
        }

            // Get envVar
        else if (strncmp("get", input, 3) == 0 && input[3] == ' ') {

            for (i = 4; i < strlen(input); i++) {
                envVar[i - 4] = input[i];
            }

            if (getenv(envVar) == NULL) {
                printf("Failed to find environment variable\n");
            } else {
                printf("%s\n", getenv(envVar));
            }
        }

            /*
             * NOT BUILT IN COMMANDS
             */

        else {
            // Count number of args for args[]
            int numArgs = 1;
            for (i = 0; i < strlen(input); i++) {
                if (input[i] == ' ') {
                    numArgs++;
                }
            }

            // Check if last arg is '&' and flag as background process as needed
            if (input[strlen(input) - 1] == '&') {
                numArgs--;
                backProc = 1;
            }

            /*fill args[] using temp string x for each individual argument*/
            char temp[numArgs][MAXINPUTSIZE];
            char *args[numArgs + 1];
            numArgs = 0;
            k = 0;
            for (i = 0; i < strlen(input) + 1; i++) {
                if (input[i] == ' ' || input[i] == '\0') {
                    temp[numArgs][k] = '\0';
                    args[numArgs] = temp[numArgs];
                    numArgs++;
                    k = 0;
                } else if (input[i] == '&' && i == strlen(input) - 1) {
                    i = (int) strlen(input) + 1;
                } else {
                    temp[numArgs][k] = input[i];
                    k++;
                }
            }

            // NULL pointer at end of args
            args[numArgs] = (char *) NULL;

            // If background process, spawn child (child2) that spawns another child (child) to run the process
            // Main process continues to loop and re-prompts for user input
            if (backProc == 1) {

                backProc = 0;
                child2 = fork();

                if (child2 == 0) {

                    child = fork();

                    if (child == 0) {
                        // print process ID and command name
                        printf("[%d] %s\n", getpid(), args[0]);
                        // Execute
                        execvp(args[0], args);
                        // If we get this far...it's an error
                        perror("\0");
                        return 0;

                    } else {
                        status = -1;
                        // child2 waits for child to complete
                        waitpid(-1, &status, 0);
                        // Print completed child process ID and return status
                        printf("\n[%d] %s Exit %d\n", child, args[0], WEXITSTATUS(status));
                        fprintf(stderr, "%s", prompt);
                        return 0;
                    }
                } else {
                    usleep(1000);
                }
            }

                // Not a background process, spawn child (child) that runs command
            else {
                // Create child process for command
                child = fork();
                // If child == 0, we are in the child process
                if (child == 0) {
                    // Print process ID and command name
                    printf("[%d] %s\n", getpid(), args[0]);
                    // Execute command
                    execvp(args[0], args);
                    // If we get this far...it's an error
                    perror("\0");
                    return 0;
                }
                    // Parent process
                else {
                    usleep(1000);
                    // Init status and wait for child to complete
                    status = -1;
                    waitpid(child, &status, 0);
                    // Print the completed child process ID name and return status
                   printf("[%d] %s Exit %d\n", child, args[0], WEXITSTATUS(status));
                }
            }
        }
    }
}
