/* t2.c
   synchronize threads through mutex and conditional variable
   To compile use: gcc -o t2 t2.c -lpthread
*/

#include <stdio.h>
#include <pthread.h>

// Prototypes
void hello();

void world();

void again();

// Global variable shared by threads
pthread_mutex_t mutex;      // mutex
pthread_cond_t done_hello;  // Hello conditional variable
pthread_cond_t done_world;  // World conditional

// Testing variables
int hello_done = 0;
int world_done = 0;

int main(int argc, char *argv[]) {

    /*
     * Initializations
     */

    // Thread IDs
    pthread_t tid_hello, tid_world, tid_again;

    // Mutex and cond varsinits
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&done_hello, NULL);
    pthread_cond_init(&done_world, NULL);

    // Thread creation
    pthread_create(&tid_hello, NULL, (void *) &hello, NULL);
    pthread_create(&tid_world, NULL, (void *) &world, NULL);
    pthread_create(&tid_again, NULL, (void *) &again, NULL);

    /* main waits for the threads to finish */
    pthread_join(tid_hello, NULL);
    pthread_join(tid_world, NULL);
    pthread_join(tid_again, NULL);

    printf("\n");
    return 0;
}

void hello() {
    pthread_mutex_lock(&mutex);
    printf("hello ");
    fflush(stdout);    // flush buffer to allow instant print out
    hello_done = 1;
    pthread_cond_signal(&done_hello);    // signal world() thread
    pthread_mutex_unlock(&mutex);    // unlocks mutex to allow world to print
    return;
}


void world() {
    pthread_mutex_lock(&mutex);

    // world thread waits until done == 1.
    while (hello_done == 0)
        pthread_cond_wait(&done_hello, &mutex);

    printf("world ");
    fflush(stdout);
    world_done = 1;
    pthread_cond_signal(&done_world);  // signal again() thread
    pthread_mutex_unlock(&mutex); // unlocks mutex

    return;
}

void again() {
    pthread_mutex_lock(&mutex);

    // again thread waits til done_world == 1
    while (world_done == 0) {
        pthread_cond_wait(&done_world, &mutex);
    }

    printf("again");
    fflush(stdout);
    pthread_mutex_unlock(&mutex); // unlocks mutex

    return;
}
