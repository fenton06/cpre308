/*******************************************************************************
*
* CprE 308 Scheduling Lab
*
* scheduling.c
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_PROCESSES 20

struct process {
    /* Values initialized for each process */
    int arrivaltime;  /* Time process arrives and wishes to start */
    int runtime;      /* Time process requires to complete job */
    int priority;     /* Priority of the process */

    /* Values algorithm may use to track processes */
    int starttime;
    int endtime;
    int flag;
    int remainingtime;
};

/* Forward declarations of Scheduling algorithms */
void first_come_first_served(struct process *proc);

void shortest_remaining_time(struct process *proc);

void round_robin(struct process *proc);

void round_robin_priority(struct process *proc);

int main() {
    int i;
    struct process proc[NUM_PROCESSES],      /* List of processes */
            proc_copy[NUM_PROCESSES]; /* Backup copy of processes */

    /* Seed random number generator */
    /*srand(time(0));*/  /* Use this seed to test different scenarios */
    srand(0xC0FFEE);     /* Used for test to be printed out */

    /* Initialize process structures */
    for (i = 0; i < NUM_PROCESSES; i++) {
        proc[i].arrivaltime = rand() % 100;
        proc[i].runtime = (rand() % 30) + 10;
        proc[i].priority = rand() % 3;
        proc[i].starttime = 0;
        proc[i].endtime = 0;
        proc[i].flag = 0;
        proc[i].remainingtime = 0;
    }

    /* Show process values */
    printf("Process\tarrival\truntime\tpriority\n");
    for (i = 0; i < NUM_PROCESSES; i++)
        printf("%d\t%d\t%d\t%d\n", i, proc[i].arrivaltime, proc[i].runtime,
               proc[i].priority);

    /* Run scheduling algorithms */
    printf("\n\nFirst come first served\n");
    memcpy(proc_copy, proc, NUM_PROCESSES * sizeof(struct process));
    first_come_first_served(proc_copy);

    printf("\n\nShortest remaining time\n");
    memcpy(proc_copy, proc, NUM_PROCESSES * sizeof(struct process));
    shortest_remaining_time(proc_copy);

    printf("\n\nRound Robin\n");
    memcpy(proc_copy, proc, NUM_PROCESSES * sizeof(struct process));
    round_robin(proc_copy);

    printf("\n\nRound Robin with priority\n");
    memcpy(proc_copy, proc, NUM_PROCESSES * sizeof(struct process));
    round_robin_priority(proc_copy);

    return 0;
}

void first_come_first_served(struct process *proc) {

    int i, j;

    // Completion time running total, average, and system time
    int compTimeTotal = 0;
    int avgCompTime;
    int sysTime = 0;

    // First process indice
    int firstProc;

    // Execute processes
    for (i = 0; i < NUM_PROCESSES; i++) {
        // Init first process
        firstProc = i;

        // Find first process
        for (j = 0; j < NUM_PROCESSES; j++) {
            // If first proc is complete, flag it
            if (proc[firstProc].flag) {
                firstProc = j;
            }
                // Overwrite firstProc if it's arrival time is after current process
            else if (proc[firstProc].arrivaltime > proc[j].arrivaltime && !proc[j].flag) {
                firstProc = j;
            }
        }

        // Advance sys time if it isn't saqme as current time
        if (sysTime < proc[firstProc].arrivaltime) {
            sysTime = proc[firstProc].arrivaltime;
        }


        /*
         * Execute process
         */

        // Set start time, advance sys time, set end time, and track of total runtime
        proc[firstProc].starttime = sysTime;
        sysTime += proc[firstProc].runtime;
        proc[firstProc].endtime = sysTime;
        compTimeTotal += (proc[firstProc].endtime - proc[firstProc].arrivaltime);

        // Mark firstProc as completed
        proc[firstProc].flag = 1;

        // Print start and finish time
        printf("Process %d started at time %d\n", firstProc, proc[firstProc].starttime);
        printf("Process %d finished at time %d\n", firstProc, proc[firstProc].endtime);
    }

    // Calculate average completion time and print
    avgCompTime = compTimeTotal / NUM_PROCESSES;

    printf("Average time from arrival to completion is %d seconds\n", avgCompTime);
}

void shortest_remaining_time(struct process *proc) {

    int i, j;

    // Completion time running total, average, and system time
    int compTimeTotal = 0;
    int avgCompTime;
    int sysTime = 0;

    // Shortest remaining time indice
    int shortTime;

    // Execute processes
    for (i = 0; i < NUM_PROCESSES; i++) {
        // Init shortTime
        shortTime = -1;

        // Find shortest remaining time
        for (j = 0; j < NUM_PROCESSES; j++) {
            // Shortest time has not been set, arrival time is <= sysTime, and current process hasn't finished
            if (shortTime < 0 && proc[j].arrivaltime <= sysTime && !proc[j].flag) {
                shortTime = j;
            }

                /* Shortest remaining time set, current process has arrived, current process runtime is shorter than
                 * shortest remaining time, and current process hasn't finished, overwrite it */
            else if (shortTime >= 0 && proc[j].arrivaltime <= sysTime &&
                     proc[j].runtime < proc[shortTime].runtime && !proc[j].flag) {
                shortTime = j;
            }
        }

        // No process found; advance system time and continue
        if (shortTime < 0) {
            sysTime++;
            i--;
            continue;
        }


        /*
         * Execute process
         */

        // Set startTime, advance sysTime, and set endTime, track total runtime
        proc[shortTime].starttime = sysTime;
        sysTime += proc[shortTime].runtime;
        proc[shortTime].endtime = sysTime;
        compTimeTotal += (proc[shortTime].endtime - proc[shortTime].arrivaltime);

        // Mark shortTime as completed
        proc[shortTime].flag = 1;

        // Print process start and finish
        printf("Process %d started at time %d\n", shortTime, proc[shortTime].starttime);
        printf("Process %d finished at time %d\n", shortTime, proc[shortTime].endtime);
    }

    // Calculate average completion time and pring
    avgCompTime = compTimeTotal / NUM_PROCESSES;

    printf("Average time from arrival to completion is %d seconds\n", avgCompTime);
}

void round_robin(struct process *proc) {

    int i, j = 0;

    // First process ID searched at sysTime
    int jStart = 0;

    // Total completion time, average completion time, and system time
    int compTimeTotal = 0;
    int avgCompTime;
    int sysTime = 0;

    // Process finished flag
    int procFinished;

    // Complete all processes
    for (i = 0; i < NUM_PROCESSES; i++) {
        // Init procFinished
        procFinished = 0;

        while (!procFinished) {
            // Run process for one second if not completed
            if (proc[j].arrivaltime <= sysTime && proc[j].flag != 2) {
                // Init proc[j] if it just started running
                if (!proc[j].flag) {
                    proc[j].flag = 1;
                    proc[j].starttime = sysTime;
                    proc[j].remainingtime = proc[j].runtime - 1;
                }

                    // Update process
                else {
                    proc[j].remainingtime--;

                    // Process finished
                    if (!proc[j].remainingtime) {
                        proc[j].flag = 2;
                        proc[j].endtime = sysTime + 1;
                        procFinished = 1;
                        compTimeTotal += (proc[j].endtime - proc[j].arrivaltime);
                        printf("Process %d started at time %d\n", j, proc[j].starttime);
                        printf("Process %d finished at time %d\n", j, proc[j].endtime);
                    }
                }

                // Update j and increment system time
                j = (j < (NUM_PROCESSES - 1)) ? (j + 1) : 0;
                sysTime++;
                jStart = j;
            }
                // proc[j] can't be ran
            else {
                // Update j
                j = (j < (NUM_PROCESSES - 1)) ? (j + 1) : 0;

                // Incrememnt system time if no process can be ran if j == jStart
                if (j == jStart)
                    sysTime++;
            }
        }
    }

    // Calculate average completion time and print
    avgCompTime = compTimeTotal / NUM_PROCESSES;

    printf("Average time from arrival to completion is %d seconds\n", avgCompTime);
}

void round_robin_priority(struct process *proc) {

    int i, j = 0;

    // First process ID searched at sysTime
    int jStart = 0;

    // Total completion time, average completion time, and system time
    int compTimeTotal = 0;
    int avgCompTime;
    int sysTime = 0;

    // Process finished flag
    int procFinished;

    // Highest priority
    int highestPriority;

    // Last executed process
    int lastExec = 0;

    // Complete all processes
    for (i = 0; i < NUM_PROCESSES; i++) {
        // Init procFinished
        procFinished = 0;

        while (!procFinished) {
            // Init highestPriority
            highestPriority = -1;

            do {
                // Set highestPriority if proc[j] has arrived and highestPriority has not been set
                if (proc[j].arrivaltime <= sysTime && highestPriority < 0
                    && proc[j].flag < 2) {
                    highestPriority = j;
                }
                    // Update highestPriority if proc[j] has arrived, is not done, and is of higher priority than current
                else if (proc[j].arrivaltime <= sysTime && proc[j].flag < 2 && proc[j].priority > proc[highestPriority].priority) {
                    highestPriority = j;
                }

                if(j < (NUM_PROCESSES - 1)) {
                    j++;
                } else {
                    j = 0;
                }

            } while (j != jStart);

            // Execute if highestPriority has been set
            if (highestPriority > -1) {
                lastExec = highestPriority;

                // Init if just starting
                if (!proc[highestPriority].flag) {
                    proc[highestPriority].flag = 1;
                    proc[highestPriority].starttime = sysTime;
                    proc[highestPriority].remainingtime = proc[highestPriority].runtime - 1;
                }

                    // Update process
                else {
                    proc[highestPriority].remainingtime--;

                    // Update if finished
                    if (!proc[highestPriority].remainingtime) {
                        proc[highestPriority].flag = 2;
                        proc[highestPriority].endtime = sysTime + 1;
                        procFinished = 1;
                        compTimeTotal += (proc[highestPriority].endtime - proc[highestPriority].arrivaltime);

                        // Print start and end time
                        printf("Process %d started at time %d\n", highestPriority, proc[highestPriority].starttime);
                        printf("Process %d finished at time %d\n", highestPriority, proc[highestPriority].endtime);
                    }
                }
            }

            // Increment system time
            sysTime++;

            // Set j to search from current highest priority job index
            if (lastExec == NUM_PROCESSES - 1) {
                jStart = 0;
                j = 0;
            } else {
                jStart = (lastExec + 1);
                j = (lastExec + 1);
            }
        }
    }

    // Calculate average completion time and print
    avgCompTime = compTimeTotal / NUM_PROCESSES;

    printf("Average time from arrival to completion is %d seconds\n", avgCompTime);
}
