#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void my_routine();

int main() {

    signal(SIGQUIT, my_routine);

    printf("Entering infinite loop\n");

    // Take an infinite number of naps
    while (1) {
        sleep(10);
    }

    printf("Can’t get here\n");
}

// Will be called asynchronously, even during a sleep
void my_routine() {
    printf("Running my_routine\n");
}
