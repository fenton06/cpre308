#include <stdio.h>
#include <signal.h>

/*function prototype for division by 0 signal handler*/
void zero_division();

int main() {
    // Assign zero_division as action for signal SIGFPE
    signal(SIGFPE, zero_division);

    // Divide a by 0
    int a = 4;
    a = a / 0;

    return 0;
}

// Divide by 0 handler
void zero_division() {
    printf("caught a SIGFPE\n");

    return;
}
